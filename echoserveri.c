#include "csapp.h"
#include "string.h"
#include <stdlib.h>



void quitNewCharacterLineInput(char *str){
	/* Remove trailing newline, if there. */
    if ((strlen(str)>0) && (str[strlen (str) - 1] == '\n'))
        str[strlen (str) - 1] = '\0';
}


void echo(int connfd)
{
	size_t n;
	char buf[MAXLINE];
	char *message;
	rio_t rio;
	//printf("%d\n", MAXLINE );
	Rio_readinitb(&rio, connfd);
	while((n = Rio_readlineb(&rio, buf, MAXLINE)) != 0) {
		pid_t pid ;
		quitNewCharacterLineInput(buf);
		char *const parmList[] = {buf, NULL};
		//message = "OK\n";
		int status;
		
		if((pid = Fork()) == 0){

			if (execve(buf, parmList , NULL)<0){
				printf("ERROR\n");
				//return EXIT_FAILURE;
				//message = "ERROR\n";
				//Rio_writen(connfd, message, strlen(message));
				
			}
			//exit(0);
		}

		if (waitpid(pid, &status, 0 ) < 0){
			message="ERROR\n";
		}else{
			message="OK\n";
		}
		
		printf("server received %lu bytes\n", n);
		//printf("lobgutud buffer vuelta escritura %zu\n", strlen(message) );
		Rio_writen(connfd, message, strlen(message));
		//memset(buf,'\0',MAXLINE);
	}
}

void sigchld_handler(int sig){
	pid_t pid;
	int status;
	while( ( pid = waitpid(-1, &status, WNOHANG) ) > 0){
		if(WIFEXITED(status)){

			//printf("\n\nHe asesinado al hijo: %d\n\n", pid);
			//printf("Not just the men... But the women and children too.\n");
		}else{
			printf("Error\n");
		}
	}
}

int main(int argc, char **argv)
{
	int listenfd, connfd;
	unsigned int clientlen;
	struct sockaddr_in clientaddr;
	struct hostent *hp;
	char *haddrp, *port;

	if (argc != 2) {
		fprintf(stderr, "usage: %s <port>\n", argv[0]);
		exit(0);
	}
	port = argv[1];

	Signal(SIGCHLD, sigchld_handler);
	listenfd = Open_listenfd(port);
	while (1) {
		clientlen = sizeof(clientaddr);
		connfd = Accept(listenfd, (SA *)&clientaddr, &clientlen);

		if(Fork() == 0){

			Close(listenfd);

			/* Determine the domain name and IP address of the client */
			hp = Gethostbyaddr((const char *)&clientaddr.sin_addr.s_addr,
						sizeof(clientaddr.sin_addr.s_addr), AF_INET);
			haddrp = inet_ntoa(clientaddr.sin_addr);
			printf("server connected to %s (%s)\n", hp->h_name, haddrp);
			echo(connfd);
			Close(connfd);
			exit(0);
		}

		

		
		Close(connfd);
	}
	exit(0);
}

